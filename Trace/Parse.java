public abstract class Parse {

    public static Token match(Scan scn, Token.Val v, Trace trace) {
        Token t = scn.cur();
        Token.Val vv = t.val;
        if (v == vv) {
            trace.print(v + " \"" + t + "\"");
            scn.adv();
        } else {
            // scn.reset(); // empty the buffer
            throw new RuntimeException
                ("match failure: expected token " + v +
                 ", got " + vv);
        }
        return t;
    }

}
