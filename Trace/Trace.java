public class Trace {

    public String indent;

    public Trace() {
        indent = "";
    }

    public Trace(String indent) {
        this.indent = indent;
    }

    public void print(String s) {
        System.out.println(indent + s);
    }

    public Trace nonterm(String s) {
        print(s);
        return new Trace(indent + "| ");
    }

}
