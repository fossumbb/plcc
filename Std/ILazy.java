/**
 * Lazy interface definition
 */

public interface ILazy<E> {

    public E cur();

    public void adv();

    public void put(E e);

}
